import React from 'react';
import './Studio.scss';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Cyclist32, EventSchedule32 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import ls from 'local-storage'

const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Cyclist32}>Regístrate ahora</Button></Link>
  } else{
    return <Link element={Link} to="/calendar"><Button renderIcon={EventSchedule32}>Calendario</Button></Link>
  }
}


class Studio extends React.Component  {
  render() {
  return (
    <>
    
      <div className="studioBox">
      <div className="bx--grid">
        <div className="bx--row">

          <div className="bx--col-sm-8 bx--col-lg-6">
            <h2 className="studioCopy">
            Studio
            </h2>
            <p>
            K2 Fit Riders mx, la misión es poder inspirarte a llevar una vida activa saludable a través de ejercicios que te permitan sentirte motivado , y al mismo tiempo creando una comunidad de personas enfocadas en la mejora constante de tu salud.
            </p>
            <br></br>
            <br></br>
            {renderAuthButton()}
          </div>
          <div className="bx--col-sm-8 bx--col-lg-6"></div>
          
          </div>

      </div>
    </div>

    <Footer />
    </>
  );
}
};
export default Studio;
