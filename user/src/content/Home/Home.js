import React from 'react';
import './Home.scss?v=2.5.0';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import ImgRooeda from './instructors-bg.png';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { ChevronRight32, Cyclist32, Launch16 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import Packs from '../../components/Packs';
import ls from 'local-storage'


const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return  <Link element={Link} to="/signup">
              <Button renderIcon={Cyclist32}>Regístrate ahora</Button>
            </Link>
  } else{
    return <Link element={Link} to="/packs">
              <Button renderIcon={Cyclist32}>Comprar paquete</Button>
           </Link>
  }
}

const Home = () => {
  return (
    <>
    <div className="heroHome">
      <div className="bx--grid bx--grid--full-width landing-page">
        <div className="heroHeadline">

          <div className="alignBottom" data-type='home'>
            <h1 >Indoor Cycling en Guaymas</h1>
            <br></br>
            {renderAuthButton()}

            <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <a href="https://www.instagram.com/k2fitridersmx/">
                      <img src={IconInstagram} alt='K2 Studio' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/K2-fitriders-mx-109862940368463">
                      <img src={IconFacebook} alt='K2 Studio' />  
                    </a>
                  </li>
                </ul>
          </div>

        </div>

          <div className="bx--row">

          <div className="bx--col-md-12 bx--col-lg-12">

          </div>
          </div>
    </div>

    </div>
    
    <div className="aboutHome">
    <div className="bx--grid">
    <div className="bx--row">
        <div className="bx--col-md-8">

            <h1>Indoor Cycling.</h1>
            <br></br>
            <br></br>
            <p>
            <strong>K2</strong> es un nuevo concepto que hace uso de una bicicleta estacionaria, 
            la clase se basa en fuerza, velocidad e intervalos. 
            A su vez, tonificarás brazos con mancuernas e incluir abdominales y lagartijas. 
            </p>
            <br></br>
            <br></br>
            <Link element={Link} to="/studio">
              <Button renderIcon={ChevronRight32}>Conoce el studio</Button>
            </Link>
            <br></br>
            <a href="https://open.spotify.com/playlist/4Jhhf1PFKFT4kU7uQr3uU3?si=WtjiIwOUR7q9vZh4NORuFw" className="spotifyButton" target="_blank" rel="noopener noreferrer">
              Playlist en Spotify
              <Launch16 className="spotifyIcon"/>
            </a>
          </div>
          <div className="bx--col-md-8">
            <img src={ImgRooeda} alt='website logo' className="imgRooeda" />
          </div>

    </div>
    </div>
    </div>
    <Packs />
    <Footer />
    </>
  );
};
export default Home;
