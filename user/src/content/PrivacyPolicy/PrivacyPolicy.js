import React from 'react';
import './PrivacyPolicy.scss';
import Footer from '../../components/Footer';


const PrivacyPolicy = () => {
  return (
    <>
    <div className="studioHero">
      <div className="bx--grid">

      <div className="bx--row">
          <div className="bx--col-sm-8 bx--col-lg-6">
            <h2 className="studioCopy">
              Aviso de privacidad
            </h2>
          </div>      
        </div>          

        <div className="bx--row">
          <div className="bx--col-sm-8 bx--col-lg-6">
            <p>
            <span>
            En <strong>K2</strong> nos comprometemos a proteger tu privacidad en línea. 
            A continuación se muestra una breve descripción de los aspectos 
            más destacados de nuestra Política de Privacidad y Seguridad así 
            como los Términos del Servicio, la cual ofrece una recopilación 
            resumida de la manera en que se utiliza la información durante el 
            proceso de compra.
            </span>
            <br></br>
            <br></br>
            Su información personal y de pago está segura. Cada vez que usted proporcione 
            información personal o de pago <span>(por ejemplo, al realizar una compra)</span> utilizamos tecnología <strong>SSL</strong> para asegurar que la información 
            esté encriptada, es decir, codificada, para que nadie más pueda leerla mientras 
            se transmite a través de internet.
            <br></br>
            <br></br>
            <span>
            Toda la información es almacenada de forma segura mediante nuestro firewall.
            </span>

            <br></br>
            <br></br>
            <strong>
            <span>DATOS RECOPILADOS</span>
            </strong>
            <br></br>
            <br></br>

            Cuando usted compra en nuestro sitio, recopilamos información personal sobre usted, tal como su nombre, dirección de correo electrónico, facturación y envío, número telefónico y selecciones de productos para que usted pueda ordenarlos con nosotros.

            <br></br>
            <br></br>

            En cuanto a la información de las tarjetas de crédito, es almacenada en nuestra base de datos bajo un formato cifrado, detrás de un firewall seguro y a prueba de hackers. Con el propósito de brindarle una mayor seguridad el número completo de la tarjeta de crédito nunca estará a la vista.

            <br></br>
            <br></br>

            Está información es almacenada con el único objetivo de acelerar el proceso 
            de pago para futuras compras, también podemos recopilar la dirección <strong>IP</strong>, 
            tipos de dominio <span>(por ejemplo, .com o .org, etc.),</span> tipo de 
            navegador, país y estado de las páginas de nuestro sitio y el período de 
            tiempo de su visita durante su visita.

            <br></br>
            <br></br>
            <strong>
            <span>MANEJO DE DATOS</span>
            </strong>
            <br></br>
            <br></br>

            En <strong>K2</strong> utilizamos y recopilamos la información del cliente únicamente con el propósito de completar sus pedidos, enviarles información promocional, mejorar el funcionamiento de nuestro sitio, fines estadísticos y administrar nuestros sistemas. No se venderá ni alquilará su información personal a terceros. 
            
            <br></br>
            <br></br>

            Todas nuestras notificaciones son enviadas vía: <a href="mailto:hola@k2firtriders.com">hola@k2firtriders.com</a>
            <br></br>
            <br></br>
            <strong>
            <span role="img" aria-label="Description of the overall image">
              COOKIES 🍪
            </span>
            </strong>
            <br></br>
            <br></br>
            Utilizamos cookies para ayudar a identificarlo cuando visite nuestra tienda y así lograr personalizar su experiencia.
            Las cookies facilitan su uso del sitio, permiten recuperar el carrito de compras anterior, hacen que el sitio funcione más suavemente y nos ayudan a  mantener un sitio seguro. 
            <br></br>
            <br></br>
            Usted no podrá comprar en nuestro sitio sin que su navegador esté configurado para aceptar cookies.
            <br></br>
            <br></br>
            </p>
            <br></br>
            <br></br>
            <br></br>
            <p>
                <strong><span>Última actualización:</span> </strong>15 de Junio de 2020  
            </p>
            <br></br>
            <br></br>
            <br></br>
          </div>
          <div className="bx--col-sm-8 bx--col-lg-6"></div>
          
          </div>



      </div>
    </div>

    <Footer />
    </>
  );
};
export default PrivacyPolicy;
