import React from 'react';
import ls from 'local-storage'

class Logout extends React.Component {

  render() {
    ls.clear();
    window.location.href='/';
    return (
      <div > </div >
    );
  }
}

export default Logout;