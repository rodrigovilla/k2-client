import React from 'react';
import './Instructors.scss';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Cyclist32, EventSchedule32 } from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import Footer from '../../components/Footer';
import ls from 'local-storage'



const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Cyclist32}>Regístrate ahora</Button></Link>
  } else{
    return <Link element={Link} to="/calendar"><Button renderIcon={EventSchedule32}>Calendario</Button></Link>
  }
}



class Instructors extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/instructor/all`)
      .then(response => this.setState({ data: response.data }))
      .catch();
  }

  render() {
  return (
    <>
    <div className="instructorsHero">
      <div className="bx--grid">
        <div className="bx--row">

          <div className="bx--col-sm-8 bx--col-lg-6">
            <h1 className="studioCopy">
              Instructores
            </h1>
            <p>
            Somos Karla y Kathy, socias fundadoras de <strong>K2 Fit Riders mx</strong> Health Coaches, 
            amantes de la vida, la vida en equilibro y las nuevas experiencias.
            </p>
            <br></br>
            <br></br>
            {renderAuthButton()}
          </div>
          <div className="bx--col-sm-8 bx--col-lg-6"></div>
          
          </div>

          <div className="bx--row">
          <div className="bx--col-md-12 bx--col-lg-12">
                <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <a href="https://www.instagram.com/rooedastudio/">
                      <img src={IconInstagram} alt='Rooeda™ Studio' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com">
                      <img src={IconFacebook} alt='Rooeda™ Studio' />  
                    </a>  
                  </li>
                </ul>
          </div>          
          </div>

          <div className="row">
          { this.state.data.map( (element, index) => {
            return(
              <div key={index} className="instructorCopy">
              <div className="box--col--sm-6 bx--col-lg-6">
                {(element.ImagePath) ?
                <img
                    className="instructorAvatar" 
                    src={element.ImagePath} alt='Rooeda™ Studio' 
                />
                : null}
              </div>
              <div className="box--col--sm-6 bx--col-lg-6">
                <h1>{element.Name}</h1>
                <p>{element.Description}</p>
                <br></br>
                <h4>Música</h4>
                <h3>{element.Music}</h3>
              </div>
              </div>
            );

          })
          }
          </div>

      </div>
    </div>

    <Footer />
    </>
  );
}
};
export default Instructors;
