import React from 'react';
import './Footer.scss?v=2.0.0';
import IconRooeda from './logo.png';
import IconLocked from './icon-locked.svg';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Cyclist32 } from '@carbon/icons-react';
import ls from 'local-storage'

const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/login">Iniciar sesión</Link> 
  } else{
    return <Link element={Link} to="/logout">Cerrar sesión</Link> 
  }
}

const footerAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Cyclist32}>Regístrate ahora</Button></Link>
  } else{
    return <Link element={Link} to="/packs"><Button renderIcon={Cyclist32}>Comprar paquete</Button></Link>
  }
}


const Footer = () => {
    return (

      <>
      <div className="footerStyle">
        <div className="bx--grid bx--grid--full-width landing-page">
          <div className="bx--row ">
            <div className="bx--col-md-4 bx--col-lg-4">
                <img src={IconRooeda} alt='website logo' className="footerIconRooeda" />
                <h2>Fit Riders Studio</h2>
                <p>
                   Somos un studio de indoor cycling en donde 
                   <strong> vamos </strong>
                   al ritmo de la música.
                </p>
                <br></br>
                <br></br>
                {footerAuthButton()}
            </div>

            <div className="bx--col-md-4 bx--col-lg-4">
                <ul className="footerMenu">
                  <li><Link element={Link} to="/studio">Studio</Link></li>
                  <li><Link element={Link} to="/instructors">Instructores</Link></li>
                  <li><Link element={Link} to="/packs">Paquetes</Link></li>
                  <li><Link element={Link} to="/calendar">Calendario</Link></li>
                  <li>{renderAuthButton()}</li>
                </ul>
            </div>
          
            <div className="bx--col-md-4 bx--col-lg-4 footerContact">
                
                <h3>Atención a clientes:</h3> 
                <br></br>
                <p>
                  <strong>Lunes</strong> a <strong>Sábado</strong>
                  <br></br>
                  5 - 10 AM / 5 - 8 PM
                  <br></br>
                  <br></br>
                  <strong>622</strong> 149 4138
                  <br></br>
                  hola@K2fitriders.com
                </p>
                <p className="footerAddress">
                  <br></br>
                  <strong>Carretera Internacional</strong> KM 1982
                  <br></br>
                  Local 4 C, Petrolera
                  <br></br>
                  Guaymas, Sonora
                </p>
                <ul className="socialNetworks">
                  <li>
                    <a href="https://www.instagram.com/k2fitridersmx/">
                      <img src={IconInstagram} alt='K2 Studio' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/K2-fitriders-mx-109862940368463">
                      <img src={IconFacebook} alt='K2 Studio' />  
                    </a>
                  </li>
                </ul>
            </div>
            </div>

            <div className="bx--row footerRooeda">
              <div className="bx--col-md-6 bx--col-lg-6">
                  <img src={IconLocked} alt='K2 Studio' className="footerIconLocked" />
                  <p>Protegido con SSL <span>|</span>2020 K2 Fit &amp; Ride</p>
              </div>
              <div className="bx--col-md-6 bx--col-lg-6">
                  <Link element={Link} to="/terms-and-conditions">
                    Condiciones de uso
                  </Link>
              </div>
            </div>

        </div>
      </div>
      </>
    );
  };
  export default Footer;