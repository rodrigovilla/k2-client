# k2-client-admin

## How to Run

>>>
1. Clone project
  * git clone https://gitlab.com/rodrigovilla/k2-client.git

2. Install and Run K2-Api
  * [https://gitlab.com/VEnriquez89010/k2-api](https://gitlab.com/VEnriquez89010/k2-api)

3. Run project
  * cd k2-client/admin
  * npm install
  * npm start


4. Listen on port 3000, examples
  * Open [http://localhost:3006](http://localhost:3006) to view it in the browser.
>>>


## Production

>>>
* npm run prebuild
* npm run build
* node server.js
* Open [http://localhost:8001](http://localhost:8001) to view it in the browser.
>>>

