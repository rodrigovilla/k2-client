import React from 'react';
import './Header.scss?v=2.0.0';
import Logo from './logo.png';
import ls from 'local-storage'
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Link } from 'react-router-dom';

class Header extends React.Component {
    constructor(props) {
        super(props);
        const path = window.location.href.split('/').pop();
        this.state = {currentPath: path};
      }

        render() {
            let session = ls.get('session');
            let name = ls.get('name');

            if(this.state.currentPath !== 'login' && !session){
                window.location.href = '/login';
                return 
            }

            const renderAuthButton = ()=>{
                    return (
                        <div className="sessionControl">
                        <Link element={Link} to="/" className="userName">
                            <strong>{name}</strong> 
                        </Link>
                        <OverflowMenu className="menu" flipped> 
                            <OverflowMenuItem
                                itemText="Cerrar sesión"
                                
                                primaryFocus
                                direction='top'
                                onClick={ function logout () {
                                    window.location.href='/logout';
                                }}
                                />
                        </OverflowMenu>
                        </div>
                    )
                }

            return (
                <>
                {(session) ?
                    <div>
                        <Link element={Link} to="/">
                            <img src={Logo} alt='website logo' className="logo" />
                        </Link>
                        {renderAuthButton()}
                    </div>
                :
                    <div>
                        <Link element={Link} to="/login">
                            <img src={Logo} alt='website logo' className="logo" />
                        </Link>
                    </div>
                }
                </>
            )
        }
}
export default Header;
