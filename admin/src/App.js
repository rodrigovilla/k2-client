import React, { Component } from 'react';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Calendar from './components/Calendar';
import Home from './content/Home';
import Profile from './content/Profile';
import Signup from './content/Signup';
import Login from './content/Login';
import ForgotPass from './content/ForgotPass';
import NewPass from './content/NewPass';
import Instructors from './content/Instructors';
import Members from './content/Members';
import Reservations from './content/Reservations';
import AddInstructor from './content/AddInstructor';
import Logout from './content/Logout';
import ClassRoom from './content/ClassRoom';
import AddClassRoom from './content/AddClassRoom';
import CacheBuster from './CacheBuster';

class App extends Component {
  render() {
    return (
      <>

        <Content>
        <Sidebar></Sidebar>
        <Header></Header>
        <CacheBuster>
        {({ loading, isLatestVersion, refreshCacheAndReload }) => {
          if (loading) return null;
          if (!loading && !isLatestVersion) {
            refreshCacheAndReload();
          }

          return null;
        }}
      </CacheBuster>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/profile" component={Profile} />
            <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} />
            <Route path="/logout" component={Logout} />
            <Route path="/forgotpass" component={ForgotPass} />
            <Route path="/newpass" component={NewPass} />
            <Route path="/instructors" component={Instructors} />
            <Route path="/addinstructor" component={AddInstructor} />
            <Route path="/calendar" component={Calendar} />
            <Route path="/members" component={Members} />
            <Route path="/reservations" component={Reservations} />
            <Route path="/class-room" component={ClassRoom} />
            <Route path="/add-class-room" component={AddClassRoom} />
          </Switch>
        </Content>
      </>
    );
  }
}

export default App;
