import React from 'react';
import './Members.scss?v=2.0.0';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'
import { Button, Search } from 'carbon-components-react';

class Members extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: [], auxList: [], text: ''};
  }

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/user-pack/all-available`)
    .then(users =>  this.setState({ data: users.data, auxList: users.data }))
    .catch(error => this.setState({ error, isLoading: false }));
}


  datediff(ex) {
    var now = new Date();
    var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var expire = new Date(ex);
    return Math.round((expire-today)/(1000*60*60*24));
  }

  filter(text){
    if(text.target.value){
      var result = this.state.data.filter(x => {
        return ((x.Name) ? x.Name.toLowerCase().includes(text.target.value.toLowerCase()) : false)
        || ((x.Email)? x.Email.toLowerCase().includes(text.target.value.toLowerCase()): false)
      });

      this.setState({auxList: result})
      return;
    }

    this.setState({ auxList: this.state.data })
  }


  render(){
    return (
      <div className="dashBoard">
      <div className="formStyle">
        <div className="bx--grid">
        <div className="bx--row">
            <div className="bx--col-lg-3"></div>
            <div className="bx--col-lg-6">
            <Search
                  id="search-1"
                  name='search'
                  placeHolderText="Buscar"
                  light
                  onChange={text => this.filter(text)}
                />
                <br></br>
            </div>
            <div className="bx--col-lg-3"></div>
          </div>

          <div class="bx--row">
                    <div class="bx--col-md-2"></div>
                    <div class="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Nombre
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Expiración
                      </p>
                    </div>
                    <div class="bx--col-md-2"></div>
          </div>
          
          { this.state.auxList.map( (element, index) => {
                return(
                  <div key={index} className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-2 trainerRow" >
                    <p 
                      className="instructorsLink"
                      onClick={ () => {
                      ls.set('memberName', element.Name );
                      window.location.href=`/reservations/${element.Id}`;
                      }}
                        >{element.Name}</p>
                  </div>
                  <div className="bx--col-md-2 trainerRow" >
                  { ( this.datediff(element.Expire) > 0 )
                    ?
                    <p className="expire">{this.datediff(element.Expire)} {( this.datediff(element.Expire) === 1 ) ? 'día' : 'días'  }</p>
                    : 
                    <p className="expire">Expirado</p>
                    }
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                );
              })
          }
        </div>
        <br></br><br></br><br></br>
        <div class="bx--row">
        <div class="bx--col"> </div>
        <div class="bx--col">
              <Button 
                  type="submit" 
                  value="Submit" 
                  className="buttonAccess"
                  size="small"
                  kind="secondary"
                  onClick={() => window.location.href = "/"}
              >
                  Regresar
              </Button>
              </div>
        <div class="bx--col"></div>
        </div>
        </div>
      </div>
    );
  };
}

export default Members;
